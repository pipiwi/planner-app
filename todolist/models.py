from django.db import models

# Create your models here.
class ToDoList(models.Model):
    Plan = models.CharField(max_length=50)
    Date = models.DateField(max_length = 10, default="Unknown")

    def __str__(self):
        return "{}{}".format(self.Plan, self.Date)

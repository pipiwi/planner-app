from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .models import ToDoList
from django.apps.registry import Apps
from .forms import PlannerForm
from .views import plan, delete_plan


# Create your tests here.
class PlannerUnitTest(TestCase):

    def test_url_plan(self):
        response = Client().get('/todolist/')
        self.assertEquals(302, response.status_code)

    # def test_template_plan(self):
    #     response = Client().get('/todolist/')
    #     self.assertTemplateUsed(response, 'planner/todolist.html')
    
    # def test_content_plan(self):
    #     response = Client().get('/todolist/')
    #     isi_html = response.content.decode('utf8')

    #     self.assertIn('Add a plan', isi_html)
    #     self.assertIn('<h4 class="modal-title">What to Do?</h4>', isi_html)
    #     self.assertIn('<button type="submit" class="btn">Save</button>', isi_html) 

    def test_plan_func(self):
        found =  resolve('/todolist/')
        self.assertEqual(found.func, plan)

    def test_url_delete_plan(self):
        response = Client().get('/todolist/')
        self.assertEquals(302, response.status_code)
    
    # def test_template_delete_plan(self):
    #     response = Client().get('/todolist/')
    #     self.assertTemplateUsed(response, 'planner/todolist.html')

    # def test_delete_plan_func(self):
    #     found =  resolve('/todolist/delete/<1>/')
    #     self.assertEqual(found.func, delete_plan)
    
    def test_app_used(self):
        test_apps = Apps(['todolist'])

class TestPlanModels(TestCase):
    def test_plan_models(self):
        plan = ToDoList(Plan="makan", Date="")
        self.assertEqual(str(plan), "{}{}".format(plan.Plan, plan.Date))

    def test_plan_model(self):
        ToDoList.objects.create(Date="2020-11-15")
        jumlah_data = ToDoList.objects.all().count()
        self.assertEquals(jumlah_data, 1)
    
    def test_model_name(self):
        ToDoList.objects.create(Date="2020-11-15")
        plan = ToDoList.objects.get(Date="2020-11-15")
        self.assertEqual(str(plan),'2020-11-15')


class TestPlanForm(TestCase):
    def test_form_input(self):
        form = PlannerForm()
        self.assertIn('type="text"', form.as_p())

    def test_form_save(self):
        form = PlannerForm(data={"Plan":""})
        self.assertEqual(form.errors["Plan"], ["This field is required."])

    def test_form_is_invalid(self):
        form = PlannerForm(data={})
        self.assertFalse(form.is_valid())

class TestDelete(TestCase):
    def setUp(self):
        self.plan = ToDoList(
            Plan="makan",
            Date="2020-11-15",
        )
        self.plan.save()

    def test(self):
        response2 = Client().post('/todolist/')    
        self.assertEqual(response2.status_code, 302)

    def test_delete(self):
        response = self.client.get('/todolist/delete/1')
        self.assertEqual(response.status_code, 302)
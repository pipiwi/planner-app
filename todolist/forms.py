from django import forms
from .models import ToDoList

class PlannerForm(forms.ModelForm):
    class Meta:
        model = ToDoList
        fields = ['Plan']

        widgets = {
            'Plan':forms.TextInput(attrs={'class': 'form-control'}),
        }

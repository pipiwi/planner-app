from django.urls import path, include
from . import views

app_name = 'todolist'
urlpatterns = [
    path('', views.plan, name="plan"),
    path('delete/<id>', views.delete_plan, name="delete_plan"),
]

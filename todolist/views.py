from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from .models import ToDoList
from .forms import PlannerForm

# Create your views here.
def plan(request):
    if request.user.is_authenticated:
        form = PlannerForm()
        plans = {'form':form, 'all_plans':ToDoList.objects.all().order_by('Date')}
        if request.method == 'POST':
            planform = PlannerForm(request.POST)
            plan = planform.data['Plan']
            date = request.POST.get('date')
            hasil = ToDoList(Plan=plan, Date=date)
            hasil.save()

        return render(request, 'planner/todolist.html', plans)

    else:
        return HttpResponseRedirect('/')

def delete_plan(request, id):
    target_delete = ToDoList.objects.get(id=id)
    target_delete.delete()
    return redirect('todolist:plan')
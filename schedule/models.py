from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# from accounts import models

class ScheduleModels(models.Model):

    # userlink = models.OneToOneField(User, on_delete=models.CASCADE, default=None)

    day         = models.CharField(max_length = 50)
    kelas       = models.CharField(max_length = 50)
    period      = models.CharField(max_length = 50)
    time        = models.CharField(max_length = 50)
    lecturer    = models.CharField(max_length = 50)
    
    def __str__(self):
        return "{}. {}".format(self.id, self.kelas)




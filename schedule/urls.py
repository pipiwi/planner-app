from django.contrib import admin
from django.urls import path
from . import views 

urlpatterns = [
    path('schedule-exists/', views.validate_class, name='validate_class'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
    path('', views.schedule, name='schedule'),
]

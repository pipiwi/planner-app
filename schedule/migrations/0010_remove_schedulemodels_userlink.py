# Generated by Django 3.1.2 on 2021-01-02 09:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0009_schedulemodels_userlink'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='schedulemodels',
            name='userlink',
        ),
    ]

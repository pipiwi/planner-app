from django import forms
from .models import ScheduleModels

class ScheduleForms(forms.ModelForm):
    class Meta:
        model = ScheduleModels
        fields = [
            'day',
            'kelas',
            'period',
            'time',
            'lecturer',
        ]


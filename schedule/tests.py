from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import schedule, delete
from .models import ScheduleModels
from .forms import ScheduleForms
from django.apps.registry import Apps
from .apps import ScheduleConfig
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

# Create your tests here.

# ==================SCHEDULE=====================
class TestSchedule(TestCase):
    def test_url_schedule(self):
        response = Client().get('/schedule/')
        self.assertEquals(302, response.status_code)    # redirect, krn blm login

    # def test_schedule_template(self):
    #     response = Client().get('/schedule/')
    #     self.assertTemplateUsed(response, 'schedule.html')

    def test_schedule_func(self):
        found =  resolve('/schedule/')
        self.assertEqual(found.func, schedule)

#     # def test_app_used(self):
#     #     test_apps = Apps(['schedule'])

class TestScheduleModels(TestCase):
    def test_string_models(self):
        schedule = ScheduleModels(kelas="PPW")
        self.assertEqual(str(schedule), "{}. {}".format(schedule.id, schedule.kelas))

class TestForm(TestCase):
    def test_form_success(self):
        schedule = ScheduleModels(day="Monday",kelas="PPW",period="2020",time="13.00",lecturer="aaaa")
        schedule.save()

        get = Client().get('/schedule/')
        post = Client().post('/schedule/',data={'day':"Monday", 'kelas':"DDAK",'period':"2020",'time':"12.00",'lecturer':"bbbbb"})

        self.assertEqual(302, post.status_code)
        self.assertEqual(302, get.status_code)

class TestLogin(TestCase):
    def test(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        user_login = client.login(username='test', password='12test12')

        response = client.get('/schedule/')
        user = get_user_model().objects.get(username='test')
        self.assertEqual(user.email, 'test@example.com')
        self.assertEqual(response.status_code, 200)

class TestDelete(TestCase):
    def setUp(self):

        self.jadwal = ScheduleModels(
            day="Monday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test(self):
        response2 = Client().get('/schedule/')        
        self.assertEqual(response2.status_code, 302)

    def test_delete(self):
        response = self.client.get('/schedule/delete/1')
        self.assertEqual(response.status_code, 302)

class TestFormTuesday(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Tuesday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test_form_schedule(self):
        self.assertEqual(ScheduleModels.objects.all().count(), 1)

    def test(self):
        response2 = Client().post('/schedule/')        
        self.assertEqual(response2.status_code, 302)

class TestFormWednesday(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Wednesday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test_form_schedule(self):
        self.assertEqual(ScheduleModels.objects.all().count(), 1)

    def test(self):
        response2 = Client().post('/schedule/')         
        self.assertEqual(response2.status_code, 302)

class TestFormThursday(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Thursday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test(self):
        response2 = Client().post('/schedule/')         
        self.assertEqual(response2.status_code, 302)

class TestFormsFriday(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Friday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test(self):
        response2 = Client().post('/schedule/')         
        self.assertEqual(response2.status_code, 302)

class TestFormSaturday(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Saturday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test(self):
        response2 = Client().post('/schedule/')          
        self.assertEqual(response2.status_code, 302)

class TestFormSunday(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Sunday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test(self):
        response2 = Client().post('/schedule/')
        self.assertEqual(response2.status_code, 302)


class TestAjax(TestCase):
    def setUp(self):
        self.jadwal = ScheduleModels(
            day="Sunday",
            kelas="PPW",
            period="2020/2021",
            time="13.00",
            lecturer="aaaaaa",
        )
        self.jadwal.save()

    def test_ajax(self):
        response = Client().get('/schedule/schedule-exists/?kelas=PPW')
        self.assertEqual(response.status_code, 200)

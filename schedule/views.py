from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import ScheduleModels
from .forms import ScheduleForms
from django.views.generic.edit import View
import json


# Create your views here.

def schedule(request):

    # if request.user.is_authenticated:
    schedule_model = ScheduleModels.objects.all()
    # schedule_model = ScheduleModels.objects.get(userlink=request.user)
    schedule_form = ScheduleForms(request.POST or None)
    monday_schedule = []
    tuesday_schedule = []
    wednesday_schedule = []
    thursday_schedule = []
    friday_schedule = []
    saturday_schedule = []
    sunday_schedule = []
    error = None

    if request.method =='POST':
        if schedule_form.is_valid():
            schedule_form.save()

    for schedule in schedule_model:
        if schedule.day == "Monday":
            monday_schedule = [schedule]

        elif schedule.day == "Tuesday":
            tuesday_schedule = [schedule]
        
        elif schedule.day == "Wednesday":
            wednesday_schedule = [schedule]
        
        elif schedule.day == "Thursday":
            thursday_schedule = [schedule]

        elif schedule.day == "Friday":
            friday_schedule = [schedule]

        elif schedule.day == "Saturday":
            saturday_schedule = [schedule]

        elif schedule.day == "Sunday":
            sunday_schedule = [schedule]


    context = {
        'schedule_form':schedule_form,
        'schedule_model':schedule_model,
        'monday_schedule':monday_schedule,
        'tuesday_schedule':tuesday_schedule,
        'wednesday_schedule':wednesday_schedule,
        'thursday_schedule':thursday_schedule,
        'friday_schedule':friday_schedule,
        'saturday_schedule':saturday_schedule,
        'sunday_schedule':sunday_schedule,
    }

    if request.user.is_authenticated:
        return render(request, 'schedule.html', context)

    else:
        return HttpResponseRedirect('/')

def delete(request, delete_id):
    ScheduleModels.objects.filter(id=delete_id).delete()

    return HttpResponseRedirect('/schedule/')


def validate_class(request):
    cek_kelas = request.GET['kelas']

    schedule = ScheduleModels.objects.all()

    for sched in schedule:
        if sched.kelas == cek_kelas:
            data = {
                'is_exists':True
            }
    
    return JsonResponse(data)
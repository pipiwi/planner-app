from django.test import TestCase, Client
from .views import contactusform
from django.apps.registry import Apps
from .apps import ContactusConfig
from .forms import contactusForm
from .models import Contactus
from django.contrib.auth import get_user_model

class TestContactus(TestCase):
    def test_url_contactus(self):
        response = Client().get('/')
        self.assertEquals(response.status_code,200)

    # def test_template(self):
    #     response = Client().get('/contact')
    #     self.assertTemplateUsed(response,"contactus.html")

    def test_models(self):
        isi = Contactus.objects.create(contactus="impresive")
        isi.save()
        self.assertEqual(Contactus.objects.all().count(),1)

    def test_form_valid(self):
        forms = contactusForm(data={'contactus':'impresive'})
        self.assertTrue(forms.is_valid())
        self.assertEqual(forms.cleaned_data['contactus'],"impresive")

    def test_post_form(self):
        response_post = self.client.post('/',{'contactus':'impresive'})
        self.assertEqual(response_post.status_code,200)
        forms = contactusForm(data={'contactus':'impresive'})
        self.assertTrue(forms.is_valid())
        self.assertEqual(forms.cleaned_data['contactus'],"impresive")

    def test_login(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')

        
    # def test_form_save(self):
    #     form = contactusForm(data={"day":"Monday", "kelas":""})
    #     self.assertEqual(form.errors["kelas"], ["This field is required."])

    # def test_form_is_invalid(self):
    #     form = contactusForm(data={})
    #     self.assertFalse(form.is_valid())    

    
# Create your tests here.

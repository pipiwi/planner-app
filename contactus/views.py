from django.shortcuts import render
from .forms import contactusForm
from .models import Contactus
from django.shortcuts import redirect

def contactusform(request):
    if request.user.is_authenticated:
        form = contactusForm(request.POST)
        if form.is_valid():
            form.save()

        formcontact = contactusForm()
        contact1 = Contactus.objects.all()
        context = {
            'list_form':contact1 ,
            'formcontact':formcontact
        }
        return render(request,'contactus.html',context)

    else:
        return render(request, 'notlogged.html')

# Create your views here.

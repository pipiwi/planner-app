from django.forms import ModelForm
from .models import Contactus
from django import forms

class contactusForm(ModelForm):
    contactus = forms.CharField(widget=forms.Textarea, label='')
    class Meta:
        model = Contactus
        fields = ['contactus']
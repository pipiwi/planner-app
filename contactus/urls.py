from django.urls import path
from . import views
from django.conf.urls import include

app_name = 'contactus'

urlpatterns = [
    path("", views.contactusform, name='contactus')
]
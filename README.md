<h1>Tugas Kelompok 1 PPW</h1>

Anggota Kelompok G05 :
- Abdurrohim Syahruromadhon Wahyudi ( 1906307290 )
- Ahmad Fikri Rafiuddin ( 1906305814 )
- Arnindya Zhavira ( 1906308293 )
- Nadya Shelim ( 1906302970 )
- Shanika Tysha ( 1906305890 )

Aplikasi yang diajukan : E-Planner
Pada umumnya, banyak orang jarang membuat planner atau membuat list hal-hal yang harus dilakukan.
Karena itu banyak orang sering melupakan hal penting yang harus dilakukan. Maka dari itu kami memutuskan
untuk membuat aplikasi E-Planner agar dapat mempermudah seseorang untuk mengingat dan memanage task yang harus dilakukan
tanpa melewati deadline (jika ada). Selain itu, lebih efisien karena tidak perlu menulis di buku atau kertas.

Daftar fitur yang akan diimplementasi :
- Sign in / register
- Home
- To Do List
- User Recommendations
- Jadwal

link herokuapp : https://lets-plan-app.herokuapp.com/

<h5>Pipeline Status</h5>

[![pipeline status](https://gitlab.com/pipiwi/planner-app/badges/master/pipeline.svg)](https://gitlab.com/pipiwi/planner-app/-/commits/master)


<h5>Coverage</h5>

[![coverage report](https://gitlab.com/pipiwi/planner-app/badges/master/coverage.svg)](https://gitlab.com/pipiwi/planner-app/-/commits/master)
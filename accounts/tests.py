from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# Create your tests here.

class TestSignUp(TestCase):
    def test_url_signup(self):
        response = Client().get('/accounts/signup/')
        self.assertEquals(200,response.status_code)

    def test_nama_templates_signup(self):
        response = Client().get('/accounts/signup/')
        self.assertTemplateUsed(response, 'registration/signup.html') #nama html
    
    def test_isi_view_html_signup(self):
        response = Client().get('/accounts/signup/')
        html_response = response.content.decode('utf8')
        self.assertIn('<form method="post">',html_response)
        self.assertIn('<button class="button" type="submit">Create Account</button>', html_response)
    
    def setUp(self) -> None:
        self.username = 'testuser'
        self.password = 'password'
    
    def test_signup_page_view_name(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='registration/login.html')
    

    # def test_signup_function(self):
    #     self.found = resolve('/accounts/signup/')
    #     self.assertEqual(self.found.func, signup)

    # def test_login_function(self):
    #     self.found = resolve('/accounts/login/')
    #     self.assertEqual(self.found.func, login)


    
    # def test_user_authentication(self):
    #     response = self.client.post('/accounts/signup/', {'username':'tested', 'password1':'hello123', 'password2':'hello123' })
    #     self.assertIn(response['location'], '/accounts/login/')
    #     login = self.client.post('/accounts/login/', {'username':'tested', 'password':'hello123'})
    #     self.assertIn(login['location'], '/')
    #     self.client.login(username='tested',password='tester')

    
class UserCreationFormTest(TestCase):

    def test_form(self):
        data = {
            'username': 'testuser',
            'password1': 'hello123',
            'password2': 'hello123',
        }

        form = UserCreationForm(data)

        self.assertFalse(form.is_valid())

# Create your tests here.

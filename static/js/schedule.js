$(document).ready(function(){
    AOS.init({
        duration: 1200,
    });
    
    $(".delete-js").click(function(){
        var confirmation = confirm("Are you sure want to delete this schedule?");
        if (confirmation == false) {
            return false;
        }
    });

    $(".delete-js").hover(function(){
        $(this).css("background-color", "#ffbc2c");
        $(this).css("color", "white");
        $(this).css("border", "2px solid #ffbc2c");

        }, function() {
        $(this).css("background-color", "transparent");
        $(this).css("color", "#ffbc2c");
    });

    $("input").focus(function(){
        $(this).css("background-color", "#FECA59");
    });
    $("input").blur(function(){
        $(this).css("background-color", "white");
    })

    $("select").focus(function(){
        $(this).css("background-color", "#FECA59");
    });
    $("select").blur(function(){
        $(this).css("background-color", "white");
    })

    // Ajax()
    $("#kelas").change(function () {    
        var kelas = $(this).val();
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas').val('');
                    }         
                }
            }
        });
    });
    $("#kelas-tue").change(function () {     
        var kelas = $(this).val();
        console.log(kelas);
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                console.log("sama")
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas-tue').val('');
                    }            
                }
            }
        });
    });
    $("#kelas-wed").change(function () {       
        var kelas = $(this).val();
        console.log(kelas);
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                console.log("sama")
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas-wed').val('');
                    }             
                }
            }
        });
    });
    $("#kelas-thu").change(function () {       
        var kelas = $(this).val();
        console.log(kelas);
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                console.log("sama")
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas-thu').val('');
                    }             
                }
            }
        });
    });
    $("#kelas-fri").change(function () {       
        var kelas = $(this).val();
        console.log(kelas);
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                console.log("sama")
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas-fri').val('');
                    }                              }
            }
        });
    });
    $("#kelas-sat").change(function () {        
        var kelas = $(this).val();
        console.log(kelas);
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                console.log("sama")
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas-sat').val('');
                    }            
                }
            }
        });
    });
    $("#kelas-sun").change(function () {        
        var kelas = $(this).val();
        console.log(kelas);
        $.ajax({
            url: '/schedule/schedule-exists/',

            data:{
                'kelas':kelas
            },
            dataType: 'json',
            success: function (data) {
                console.log("sama")
                if (data.is_exists === true) {
                    var cek = confirm("Schedule already exists. Do you want to still continue?")  
                    if (cek == false) {
                        $('#kelas-sun').val('');
                    }                              }
            }
        });
    });
});
from django.shortcuts import render
from django.contrib.auth.models import User,auth
from todolist.models import ToDoList

# Create your views here.
def homepage(request):
    context = {}
    if request.user.is_authenticated:
        obj = ToDoList.objects.all()
        context = {
            'obj':obj
        }
        return render(request, 'home.html', context)
        
    else:
        return render(request, 'notlogged.html')

from django.test import TestCase, Client
from .views import homepage
from django.apps.registry import Apps
from todolist.models import ToDoList
from .apps import HomepageConfig
from django.contrib.auth import get_user_model


# Create your tests here.

class TestHomepage(TestCase):
    def test_url_homepage(self):
        response = Client().get('/')
        self.assertEquals(200, response.status_code)
    
    def test_obj_homepage(self):
        obj = ToDoList(Plan = '/', Date = '2020-08-12')
        obj.save()
        response = Client().get('/')
        self.assertEquals(200, response.status_code)
        self.assertEquals(ToDoList.objects.all().count(),1)
    def test_apps_homepage(self):
        apps = Apps(['homepage'])
    def test_templateViews_homepage(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'notlogged.html')
    def test_login(self):
        client = Client()
        dummy_user = get_user_model().objects.create_user(username='test', password='12test12', email='test@example.com')
        dummy_user.save()
        client.login(username='test', password='12test12')
    